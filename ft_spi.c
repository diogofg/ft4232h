#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <pthread.h>
#include "ft_spi.h"





/*******************************************************************
Function: receive_spi_byte

Thread that will read the bytes according to the SPI protocol. If
CLK is '1' and SS is '0' it means the byte is valid, so the MISO bit
is stored. If INIT is '1' it means it's a new byte, and moves on to
the next one.
*******************************************************************/
void *receive_spi_byte(void *param){
	DWORD BytesInQueue = 0;
	DWORD BytesToRead = 0;
	DWORD BytesReceived = 0;
	FT_STATUS	ftStatus;
	int i;
	int l = -1;
	int iport = *((int *) param);
	unsigned char recv_buf[RECV_BUF];
	unsigned char receive_bytes[BUF_LEN];
	unsigned char sent_bytes[BUF_LEN];
	unsigned char recv_aux;

	while(1){
		ftStatus = FT_GetQueueStatus (ftHandle[iport], &BytesInQueue);
		if(ftStatus != FT_OK) {
			printf("Error checking receive queue (error %d)\n", (int)ftStatus);
			pthread_exit(NULL);
		}
		l = -1;
		for(i=0; i<BUF_LEN; i++) receive_bytes[i] = 0;
		//While there are still bytes in queue
		while(BytesInQueue > 0){

			//If bytes in queue are greater than buffer size
			if(BytesInQueue<RECV_BUF)
				BytesToRead = BytesInQueue;
			else
				BytesToRead = RECV_BUF; 

			ftStatus = FT_Read(ftHandle[iport], recv_buf, BytesToRead, &BytesReceived);
			if(ftStatus != FT_OK) {
				printf("Failed to read from device (error %d)\n", (int)ftStatus);
			}
			//Reduce BytesInQueue only after read
			BytesInQueue = BytesInQueue - BytesReceived;

			//Setting auxiliary variables
			for(i=2; i<(int)BytesReceived; i++)
			
			//Check if clock bit is '1' and SS bit is '0'
			if((recv_buf[i]&0x01)==0x01  && (recv_buf[i]|0xFD) == 0xFD){
				//Check if this is a new byte
				if(i<2 && (recv_aux&0x02)==0x02) l++;
				else if((recv_buf[i-2] & 0x02) == 0x02) l++;//SS neg edge detect 

				sent_bytes[l] = sent_bytes[l] << 1;
				receive_bytes[l] = receive_bytes[l] << 1;
				//Check if MISO bit is '0' or '1'
				if((recv_buf[i]&0x40) == 0x40)
					receive_bytes[l] = receive_bytes[l] | 0x01;
				else
					receive_bytes[l] = receive_bytes[l] & 0xFE;
				if((recv_buf[i]&0x80) == 0x80)
					sent_bytes[l] = sent_bytes[l] | 0x01;
				else
					sent_bytes[l] = sent_bytes[l] & 0xFE;
			}
		}
		if((recv_buf[(int)BytesReceived] & 0x02)==0x02){
			l++;
			for(i=0; i<l; i++)
			if(sent_bytes[i] == 0xFF){
				received_byte[iport]=receive_bytes[i];
				receive_ind[iport]=1;
			}
		}
		recv_aux = recv_buf[(int)BytesReceived-2];
		//if(l==(size) && (recv_aux & 0x02)==0x02 )l++;
	}
	

	pthread_exit(NULL);
}

void *receive_spi_int(void *param){
	DWORD BytesInQueue = 0;
	DWORD BytesToRead = 0;
	DWORD BytesReceived = 0;
	FT_STATUS	ftStatus;
	int i;
	int l = -1;
	int iport = *((int *) param);
	unsigned char recv_buf[RECV_BUF];
	unsigned int receive_ints[BUF_LEN];
	unsigned int sent_ints[BUF_LEN];
	unsigned char recv_aux;

	thread_en[iport] = 1;

	while(thread_en[iport]==1){
		ftStatus = FT_GetQueueStatus (ftHandle[iport], &BytesInQueue);
		if(ftStatus != FT_OK) {
			printf("Error checking receive queue (error %d)\n", (int)ftStatus);
			pthread_exit(NULL);
		}
		l = -1;
		for(i=0; i<BUF_LEN; i++) receive_ints[i] = 0;
		//While there are still bytes in queue
		while(BytesInQueue > 0){

			//If bytes in queue are greater than buffer size
			if(BytesInQueue<RECV_BUF)
				BytesToRead = BytesInQueue;
			else
				BytesToRead = RECV_BUF; 

			ftStatus = FT_Read(ftHandle[iport], recv_buf, BytesToRead, &BytesReceived);
			if(ftStatus != FT_OK) {
				printf("Failed to read from device (error %d)\n", (int)ftStatus);
			}
			//Reduce BytesInQueue only after read
			BytesInQueue = BytesInQueue - BytesReceived;

			//Setting auxiliary variables
			for(i=2; i<(int)BytesReceived; i++)
			
			//Check if clock bit is '1' and SS bit is '0'
			if((recv_buf[i]&0x01)==0x01  && (recv_buf[i]|0xFD) == 0xFD){
				//Check if this is a new byte
				if(i<2 && (recv_aux&0x02)==0x02) l++;
				else if((recv_buf[i-2] & 0x02) == 0x02) l++;//SS neg edge detect 

				sent_ints[l] = sent_ints[l] << 1;
				receive_ints[l] = receive_ints[l] << 1;
				//Check if MISO bit is '0' or '1'
				if((recv_buf[i]&0x40) == 0x40)
					receive_ints[l] = receive_ints[l] | 0x00000001;
				else
					receive_ints[l] = receive_ints[l] & 0xFFFFFFFE;
				if((recv_buf[i]&0x80) == 0x80)
					sent_ints[l] = sent_ints[l] | 0x00000001;
				else
					sent_ints[l] = sent_ints[l] & 0xFFFFFFFE;
			}
		}
		if((recv_buf[(int)BytesReceived] & 0x02)==0x02){
			l++;
			for(i=0; i<l; i++)
			if(sent_ints[i] == 0xFFFFFFFF){
				received_int[iport]=receive_ints[i];
				receive_ind[iport]=1;
			}
		}
		recv_aux = recv_buf[(int)BytesReceived-2];
	}
	

	pthread_exit(NULL);
}



void spi_write_byte(unsigned char addr, unsigned char data, int iport){
	DWORD BytesSent = 0;
	FT_STATUS	ftStatus;
	int i;
	unsigned char send_buf[36];

	addr = addr & 0x7F;

	for(i=0;i<8;i++){
		send_buf[2*i] = addr<<i & 0x80;
		send_buf[2*i+1] = send_buf[2*i] | 0x01;
	}
		
		send_buf[16] = 0x02;
		send_buf[17] = 0x03;

	for(i=0;i<8;i++){
		send_buf[2*(i+9)] = data<<i & 0x80;
		send_buf[2*(i+9)+1] = send_buf[2*(i+9)] | 0x01;
	}		

		send_buf[34] = 0x02;
		send_buf[35] = 0x03;


		ftStatus = FT_Write(ftHandle[iport], idle, 2, &BytesSent);
		if(ftStatus != FT_OK) {
			printf("Failed to write to device (error %d)\n", (int)ftStatus);
			return;
		}
		//send the array
		ftStatus = FT_Write(ftHandle[iport], send_buf, 36, &BytesSent);
		if(ftStatus != FT_OK) {
			printf("Failed to write to device (error %d)\n", (int)ftStatus);
			return;
		}

	return;
}

void spi_read_byte(unsigned char addr, unsigned char *data, int iport){
	DWORD BytesSent = 0;
	FT_STATUS	ftStatus;
	int i;
	int count;
	unsigned char send_buf[18];

sendcmd:

	addr = addr | 0x80;
	receive_ind[iport]=0;
	count = 0;

	for(i=0;i<8;i++){
		send_buf[2*i] = addr<<i & 0x80;
		send_buf[2*i+1] = send_buf[2*i] | 0x01;
	}
		
	send_buf[16] = 0x02;
	send_buf[17] = 0x03;


	ftStatus = FT_Write(ftHandle[iport], idle, 2, &BytesSent);
	if(ftStatus != FT_OK) {
		printf("Failed to write to device (error %d)\n", (int)ftStatus);
		return;
	}

	//send the array
	ftStatus = FT_Write(ftHandle[iport], send_buf, 18, &BytesSent);
	if(ftStatus != FT_OK) {
		printf("Failed to write to device (error %d)\n", (int)ftStatus);
		return;
	}

	for(i=0;i<8;i++){
		send_buf[2*i] = 0x80;
		send_buf[2*i+1] = send_buf[2*i] | 0x01;
	}		

	send_buf[16] = 0x02;
	send_buf[17] = 0x03;

		
	//send the array
	ftStatus = FT_Write(ftHandle[iport], send_buf, 18, &BytesSent);
	if(ftStatus != FT_OK) {
		printf("Failed to write to device (error %d)\n", (int)ftStatus);
		return;
	}


	while(receive_ind[iport]==0){
		usleep(200);
		count++;
		if(count==5000) goto sendcmd;
	}
	if(receive_ind[iport]==1){
		*data = received_byte[iport];
		receive_ind[iport]=0;
	}

	return;
}



void spi_write_int(unsigned char addr, unsigned int data, int iport){
	DWORD BytesSent = 0;
	FT_STATUS	ftStatus;
	int i, k;
	unsigned char data_aux[4];
	unsigned char send_buf[80];

	addr = addr & 0x0F;
	addr = addr<<3;


	data_aux[0] = (char)(data>>24);
	data_aux[1] = (char)(data>>16);
	data_aux[2] = (char)(data>>8);
	data_aux[3] = (char)data;

	for(k=0;k<5;k++){
			send_buf[2*k] = (addr)<<k & 0x80;
			send_buf[2*k+1] = send_buf[2*k] | 0x01;
	}
		
		send_buf[10] = 0x02;
		send_buf[11] = 0x03;

	for(k=0;k<4;k++)
		for(i=0;i<8;i++){
			send_buf[12+2*i+16*k] = (data_aux[k])<<i & 0x80;
			send_buf[12+2*i+16*k+1] = send_buf[12+2*i+16*k] | 0x01;
		}		

		send_buf[76] = 0x02;
		send_buf[77] = 0x03;

		ftStatus = FT_Write(ftHandle[iport], idle, 2, &BytesSent);
		if(ftStatus != FT_OK) {
			printf("Failed to write to device (error %d)\n", (int)ftStatus);
			return;
		}
		//send the array
		ftStatus = FT_Write(ftHandle[iport], send_buf, 78, &BytesSent);
		if(ftStatus != FT_OK) {
			printf("Failed to write to device (error %d)\n", (int)ftStatus);
			return;
		}

	return;
}

void spi_read_int(unsigned char addr, unsigned int *data, int iport){
	DWORD BytesSent = 0;
	FT_STATUS	ftStatus;
	int i,k;
	int count;
	unsigned char send_buf[80];


	addr = addr | 0x10;
	addr = addr<<3;

sendcmd_int:
	receive_ind[iport]=0;
	count = 0;

	for(k=0;k<5;k++){
			send_buf[2*k] = (addr)<<k & 0x80;
			send_buf[2*k+1] = send_buf[2*k] | 0x01;
	}
		
	send_buf[10] = 0x02;
	send_buf[11] = 0x03;



	for(i=0;i<32;i++){
		send_buf[12+2*i] = 0x80;
		send_buf[12+2*i+1] = send_buf[12+2*i] | 0x01;
	}		

	send_buf[76] = 0x02;
	send_buf[77] = 0x03;


	ftStatus = FT_Write(ftHandle[iport], idle, 2, &BytesSent);
	if(ftStatus != FT_OK) {
		printf("Failed to write to device (error %d)\n", (int)ftStatus);
		return;
	}
	//send the array
	ftStatus = FT_Write(ftHandle[iport], send_buf, 78, &BytesSent);
	if(ftStatus != FT_OK) {
		printf("Failed to write to device (error %d)\n", (int)ftStatus);
		return;
	}


	while(receive_ind[iport]==0){
		usleep(200);
		count++;
		if(count==200) goto sendcmd_int;
	}
	if(receive_ind[iport]==1){
		*data = received_int[iport];
		receive_ind[iport]=0;
	}

	return;
}
/*******************************************************************
Function: opendev

Opens the specified device and does all the necessary configurations
such as read/write, bit mode, and baud rate

Returns 0 if successfull, or -1 on error
*******************************************************************/
int opendev_byte(int *channel, int baud){

	FT_STATUS	ftStatus;
	int iport = *channel;

	//Check if iport is a valid number
	if(iport > 3 || iport < 0){
		printf("ERROR: iport has to be an integer between 0 (channel A) and 3 (channel D).\n");
		return -1;
	}

	//Open the device and check if successful
	ftStatus = FT_Open(iport, &ftHandle[iport]);
	if(ftStatus != FT_OK) {
		//Script that will solve a common issue with opening the device
		if(ftStatus == 3) system("./rm.sh"); else goto opnerr;
		ftStatus = FT_Open(iport, &ftHandle[iport]);
		if(ftStatus != FT_OK) {
opnerr:		printf("FT_Open(%d) failed, with error %d.\n", iport, (int)ftStatus);
		return -1;
		}
	}

	ftStatus = FT_ResetDevice(ftHandle[iport]);
	if(ftStatus != FT_OK) {
		printf("Failed to reset port %d.\n", iport);
		FT_Close(ftHandle[iport]);
		return -1;
	}

	//Set read/write and bit modes
	ftStatus = FT_SetBitMode(ftHandle[iport], SPI, BITMODE);
	if(ftStatus != FT_OK) {
		printf("Failed to setup bit mode for port %d.\n", iport);
		FT_Close(ftHandle[iport]);
		return -1;
	}

	//Set baud rate
	ftStatus = FT_SetBaudRate(ftHandle[iport], baud);
	if(ftStatus != FT_OK) {
		printf("Failed to set channel %d baud rate (error %d).\n", iport, (int)ftStatus);
		FT_Close(ftHandle[iport]);
		return -1;
	}

	ftStatus = FT_Purge(ftHandle[iport], FT_PURGE_RX | FT_PURGE_TX);
	if(ftStatus != FT_OK) {
		printf("Failed to purge memory from channel %d (error %d).\n", iport, (int)ftStatus);
		FT_Close(ftHandle[iport]);
		return -1;
	}

	receive_ind[iport]=0;

	ftStatus = pthread_create(&thread_channel[iport],NULL,&receive_spi_byte, (void*)channel);
	if(ftStatus != 0) {
		printf("Failed to create thread for channel %d (error %d).\n", iport, ftStatus);
		FT_Close(ftHandle[iport]);
		return -1;
	}

	idle[0] = 0x02;
	idle[1] = 0x03;

	return 0;
}

int opendev_int(int *channel, int baud){

	FT_STATUS	ftStatus;
	int iport = *channel;

	//Check if iport is a valid number
	if(iport > 3 || iport < 0){
		printf("ERROR: iport has to be an integer between 0 (channel A) and 3 (channel D).\n");
		return -1;
	}

	//Open the device and check if successful
	ftStatus = FT_Open(iport, &ftHandle[iport]);
	if(ftStatus != FT_OK) {
		//Script that will solve a common issue with opening the device
		if(ftStatus == 3) system("./rm.sh"); else goto opnerr;
		ftStatus = FT_Open(iport, &ftHandle[iport]);
		if(ftStatus != FT_OK) {
opnerr:		printf("FT_Open(%d) failed, with error %d.\n", iport, (int)ftStatus);
		return -1;
		}
	}

	ftStatus = FT_ResetDevice(ftHandle[iport]);
	if(ftStatus != FT_OK) {
		printf("Failed to reset port %d.\n", iport);
		FT_Close(ftHandle[iport]);
		return -1;
	}

	//Set read/write and bit modes
	ftStatus = FT_SetBitMode(ftHandle[iport], SPI, BITMODE);
	if(ftStatus != FT_OK) {
		printf("Failed to setup bit mode for port %d.\n", iport);
		FT_Close(ftHandle[iport]);
		return -1;
	}

	//Set baud rate
	ftStatus = FT_SetBaudRate(ftHandle[iport], baud);
	if(ftStatus != FT_OK) {
		printf("Failed to set channel %d baud rate (error %d).\n", iport, (int)ftStatus);
		FT_Close(ftHandle[iport]);
		return -1;
	}

	ftStatus = FT_Purge(ftHandle[iport], FT_PURGE_RX | FT_PURGE_TX);
	if(ftStatus != FT_OK) {
		printf("Failed to purge memory from channel %d (error %d).\n", iport, (int)ftStatus);
		FT_Close(ftHandle[iport]);
		return -1;
	}

	receive_ind[iport]=0;

	ftStatus = pthread_create(&thread_channel[iport],NULL,&receive_spi_int, (void*)channel);
	if(ftStatus != 0) {
		printf("Failed to create thread for channel %d (error %d).\n", iport, ftStatus);
		FT_Close(ftHandle[iport]);
		return -1;
	}

	idle[0] = 0x02;
	idle[1] = 0x03;

	return 0;
}

void closedev(int iport){
	thread_en[iport]=0;
	pthread_join(thread_channel[iport], NULL);
	FT_Close(ftHandle[iport]);
	return;
}



