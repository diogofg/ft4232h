#include <stdio.h>
#include "ft_spi.h"

int main()
{
	int i;
	int iport;
	unsigned int data;

	printf("----------------------------------------------------------------------\n");
	printf("Welcome to the SPI protocol program example.\nThis example can be used to check if the devices are working properly.\n");
	printf("----------------------------------------------------------------------\n");

	printf("\nPlease select the port [0-3]:");
	scanf("%d", &iport);

	i = opendev_int(&iport, BAUD);
	if(i!=0) return 0;

	printf("\nWriting decimal \"2000\" to address 10\n");
	spi_write_int(0x0A, 2000, iport);
	spi_read_int(0x0A, &data, iport);
	printf("Received value \"%d\" from address 10\n", data);

	printf("\nWriting hexadecimal \"A500\" to address 5\n");
	spi_write_int(0x05, 0xA500, iport);
	spi_read_int(0x05, &data, iport);
	printf("Received hexadecimal value \"%X\" from address 5\n", data);



	closedev(iport);
	
	return 0;
}

